#include <iostream>
#include <fstream>
#include <vector>
#include <numeric>
#include <set>

int main(int argc, char **argv) {
	// Name of file with data is intended to be provided to the program
	if (argc < 2) {
		std::cerr << "Give me a file to read\n";
		return -1;
	}

	// Read frequency from input file data into a vector
	std::ifstream file;
	int frequencyChange;
	std::vector<int> freqChanges;
	file.open(argv[1]);
	while (file >> frequencyChange) {
		freqChanges.push_back(frequencyChange);
	}
	file.close();

	// Part one of the puzzle
	int resultPartOne = std::accumulate(freqChanges.cbegin(),
					freqChanges.cend(), 0);
	std::cout << resultPartOne << std::endl;

	// Part two of the puzzle
	std::set<int> set { };
	int total = 0;
	while (true) {
		for (int i : freqChanges) {
			total += i;
			if (auto [iter, inserted] = set.insert(total); !inserted) {
				std::cout << total;
				return 0;
			}
		}
	}

	return -2;
}
